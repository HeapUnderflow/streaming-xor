use clap::{App, AppSettings, Arg};
use std::{
	fs::File,
	io,
	io::{Read, Write},
};

const DEFAULT_BUFLEN: usize = 4096;
fn main() {
	match realmain() {
		Ok(_) => std::process::exit(0),
		Err(e) => std::process::exit(e),
	}
}

fn realmain() -> Result<(), i32> {
	let matches = App::new(env!("CARGO_PKG_NAME"))
		.author(env!("CARGO_PKG_AUTHORS"))
		.version(env!("CARGO_PKG_VERSION"))
		.about("xor all bytes from stdin or a file with the given key")
		.settings(&[
			AppSettings::ColoredHelp,
			AppSettings::ColorAuto,
			AppSettings::UnifiedHelpMessage,
		])
		.arg(
			Arg::with_name("key")
				.short("k")
				.long("key")
				.default_value("0xFF")
				.hide_default_value(false)
				.value_name("KEY")
				.takes_value(true)
				.help(
					"The key to be used. for multiple bytes, separate them with either spaces or \
					 commas. bytes are in hex",
				),
		)
		.arg(
			Arg::with_name("file")
				.required(false)
				.index(1)
				.value_name("FILE")
				.help("The file to read from, defaults to stdin, `-` works as a alias for stdin"),
		)
		.arg(
			Arg::with_name("debug")
				.required(false)
				.takes_value(false)
				.multiple(true)
				.short("v")
				.long("verbose")
				.help("Print debug information to stderr"),
		)
		.arg(
			Arg::with_name("write-file")
				.required(false)
				.takes_value(true)
				.short("o")
				.long("out")
				.help("Write the xor'd bytes to file instead of stdout, `-` works as a alias for stdout"),
		)
		.arg(
			Arg::with_name("buf-size")
				.required(false)
				.takes_value(true)
				.short("b")
				.long("bufsize")
				.help("Use a buffer of X bytes, instead of the default 4KiB"),
		)
		.get_matches();

	let debug = matches.occurrences_of("debug") >= 1;
	let trace = matches.occurrences_of("debug") >= 2;

	let buflen = match matches.value_of_lossy("buf-size") {
		Some(val) => match val.parse() {
			Ok(v) if 0 < v => v,
			Ok(_) => {
				eprintln!("bufsize cannot be 0");
				return Err(1);
			},
			Err(e) => {
				eprintln!("invalid bufsize: {:?}", e);
				return Err(1);
			},
		},
		None => DEFAULT_BUFLEN,
	};
	if debug {
		eprintln!("bufsize {}", buflen);
	}

	let key = {
		let mut key_bytes: Vec<u8> = Vec::new();
		for (idx, part) in matches
			.value_of_lossy("key")
			.unwrap()
			.split(|c| c == ' ' || c == ',' || c == '\t')
			.enumerate()
		{
			let bt = match u8::from_str_radix(&part.trim_start_matches("0x"), 16) {
				Ok(v) => v,
				Err(e) => {
					eprintln!("cannot parse key byte at position {}: {:?}", idx, e);
					return Err(1);
				},
			};
			key_bytes.push(bt);
		}

		if debug {
			eprintln!("    read key: {:02X?}", key_bytes);
		}
		key_bytes
	};

	// we are constructing a pure read object here, since we dont care where we read from
	// as long as we get bytes out of it
	let stdin = io::stdin();
	let stdout = io::stdout();
	let mut in_obj: Box<dyn Read> = match matches.value_of_lossy("file") {
		None => Box::new(stdin.lock()),
		Some(v) => {
			if v == "-" {
				Box::new(stdin.lock())
			} else {
				match File::open(&*v) {
					Ok(f) => Box::new(f),
					Err(e) => {
						eprintln!("unable to open file: {:?}", e);
						return Err(1);
					},
				}
			}
		},
	};

	// same with write: as long as it eats bytes we are happy
	let mut out_obj: Box<dyn Write> = match matches.value_of_lossy("write-file") {
		None => Box::new(stdout.lock()),
		Some(v) => {
			if v == "-" {
				Box::new(stdout.lock())
			} else {
				match File::create(&*v) {
					Ok(f) => Box::new(f),
					Err(e) => {
						eprintln!("unable to create out file: {:?}", e);
						return Err(1);
					},
				}
			}
		},
	};

	let mut buf = vec![0; buflen];
	let mut current_key_pos = 0;

	let mut next_key_byte = || {
		let b = key[current_key_pos];
		current_key_pos += 1;
		if current_key_pos >= key.len() {
			current_key_pos = 0
		}
		b
	};

	loop {
		// read as long as we dont error
		// attempt to read key.len() bytes, otherwise hack the rest of and call it EOF
		let bytes_read = match in_obj.read(&mut buf[..buflen]) {
			Ok(br) => br,
			Err(e) => {
				eprintln!("error while reading file: {:?}", e);
				return Err(1);
			},
		};

		if trace {
			eprintln!("loaded bytes: {:02X?}", &buf[..bytes_read]);
		}

		// transform the buffer inplace to avoid allocating another one
		for byte in buf.iter_mut().take(bytes_read) {
			*byte ^= next_key_byte();
		}

		if trace {
			eprintln!(" xored bytes: {:02X?}", &buf[..bytes_read]);
		}

		// write buffer to stdout
		if let Err(e) = out_obj.write(&buf[..bytes_read]) {
			eprintln!("error while writing byte: {:?}", e);
			return Err(1);
		}

		// break if we are EOF
		if bytes_read < key.len() {
			break;
		}
	}

	if debug {
		eprintln!("eof reached");
	}

	Ok(())
}

# Streaming Xor (sxor)

A Cli tool to satisfy your xor'ing needs.

```text
sxor 1.0.1
HeapUnderflow <heapunderflow@outlook.com>
xor all bytes from stdin or a file with the given key

USAGE:
    sxor [OPTIONS] [FILE]

OPTIONS:
    -b, --bufsize <buf-size>    Use a buffer of X bytes, instead of the default 4KiB
    -v, --verbose               Print debug information to stderr
    -h, --help                  Prints help information
    -k, --key <KEY>             The key to be used. for multiple bytes, separate them with either spaces or commas.
                                bytes are in hex [default: 0xFF]
    -V, --version               Prints version information
    -o, --out <write-file>      Write the xor'd bytes to file instead of stdout, `-` works as a alias for stdout

ARGS:
    <FILE>    The file to read from, defaults to stdin, `-` works as a alias for stdin
```

## Example

```text
$ echo "hello world" | sxor -k 0xFF,0xFE -vv | hexyl
bufsize 4096
    read key: [FF, FE]
loaded bytes: [68, 65, 6C, 6C, 6F, 20, 77, 6F, 72, 6C, 64, 0A]
 xored bytes: [97, 9B, 93, 92, 90, DE, 88, 91, 8D, 92, 9B, F4]
loaded bytes: []
 xored bytes: []
eof reached
+--------+-------------------------+-------------------------+--------+--------+
|00000000| 97 9b 93 92 90 de 88 91 | 8d 92 9b f4             |××××××××|××××    |
+--------+-------------------------+-------------------------+--------+--------+
```
